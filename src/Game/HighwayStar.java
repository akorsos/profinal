package Game;

import java.applet.Applet;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.util.Random;
import java.util.ArrayList;

public class HighwayStar extends Applet implements Runnable{

    // keys
    private boolean[] a = new boolean[32768];

    @Override
    public void start() {
        enableEvents(8);
        new Thread(this).start();
    }

    public void run() {

        final int SEGMENTS = 4096;
        final int ROAD = 0;
        final int HILL = 1;

        final double I256 = 0.00390625f;
        final double ACCELERATION = 0.003f;

        final int OBJ_TIME = 0;
        final int OBJ_X = 1;
        final int OBJ_DT = 2;
        final int OBJ_SPRITE = 3;

        final int OBJ_HALF_WIDTH = 10;
        final int OBJ_HEIGHT = 11;

        final int VK_LEFT = 0x25;
        final int VK_RIGHT = 0x27;
        final int VK_GAS = 0x44;
        final int VK_START = 0x20;

        int x;
        int y;
        int z;
        int i;
        int j;
        int k;
        int time = 50;
        double mag;

        boolean startReleased = true;
        boolean raceStarted = false;

        BufferedImage image = new BufferedImage(256, 256, 1);
        Graphics2D g = (Graphics2D)image.getGraphics();
        Graphics2D g2 = null;

        ArrayList<double[]> queue = new ArrayList<double[]>();
        ArrayList[] renderList = new ArrayList[8192];
        double[] player = new double[32];
        Random random = new Random(37);

        //create player
        queue.add(player);
        player[OBJ_SPRITE] = 0;
        player[OBJ_TIME] = 16;
        //player[OBJ_HALF_WIDTH] = 42;
        //player[OBJ_HEIGHT] = 54;

        for(i = 0; i < 8192; i++) {
            renderList[i] = new ArrayList<double[]>();
        }

        //generate road segments
        double[][][] segments = new double[2][SEGMENTS][3];

        //generate road image
        int[] pixels = new int[256 << 8];
        BufferedImage[] roadImages = new BufferedImage[256];
        for(y = 0; y < 256; y++) {
            for(x = 0; x < 256; x++) {
                double angle = 8 * 6.28f * x / 243f;
                mag = 0.5f + 0.5f * Math.sin(angle);
                i = 0x72 + (int)((0x93 - 0x72) * mag);
                j = 0x68 + (int)((0x8A - 0x68) * mag);
                k = 0x67 + (int)((0x83 - 0x67) * mag);
                double scale = y < 128 ? 0.95f : 1f;
                scale -= 0.05f * random.nextDouble();
                i = (int)(i * scale);
                j = (int)(j * scale);
                k = (int)(k * scale);
                pixels[x] = (i << 16) | (j << 8) | k;
            }
            for(i = 0; i < 5; i++) {
                if (i == 0 || i == 4 || y < 128) {
                    for(x = 0; x < 4; x++) {
                        pixels[x + i * 62 + 2] = i == 2 ? 0xF5C549 : 0xE7DED5;
                    }
                }
            }
            roadImages[y] = new BufferedImage(256, 1, 1);
            roadImages[y].setRGB(0, 0, 256, 1, pixels, 0, 256);
        }

        long nextFrameStartTime = System.nanoTime();
        while(true) {

            do {
                nextFrameStartTime += 16666667;

                //update starts

                if (!a[VK_START]) {
                    startReleased = true;
                }

                if (startReleased && a[VK_START]) {
                    // reset the race
                    startReleased = false;
                    raceStarted = true;
                    time = 50;

                    queue.clear();
                    player = new double[32];
                    queue.add(player);
                    player[OBJ_SPRITE] = 0;
                    player[OBJ_TIME] = 16;
//                    player[OBJ_HALF_WIDTH] = 42;
//                    player[OBJ_HEIGHT] = 54;
                }

                // update player car
                if (raceStarted && time > 0 && player[OBJ_TIME] < 2018
                        && a[VK_GAS]
                        && player[OBJ_DT] < (player[OBJ_X] > 1.25f
                        || player[OBJ_X] < -1.25f ? 0.05f : 0.15f)) {
                    player[OBJ_DT] += ACCELERATION;
                } else {
                    if (player[OBJ_DT] > ACCELERATION) {
                        player[OBJ_DT] -= ACCELERATION;
                    } else if (player[OBJ_DT] < -ACCELERATION) {
                        player[OBJ_DT] += ACCELERATION;
                    } else {
                        player[OBJ_DT] = 0;
                    }
                }

                //keeps you moving forward
                player[OBJ_TIME] += player[OBJ_DT];

                //turning
                if (a[VK_LEFT]) {
                    player[OBJ_X] -= player[OBJ_DT] * 0.167f;
                } else if (a[VK_RIGHT]) {
                    player[OBJ_X] += player[OBJ_DT] * 0.167f;
                }

                //update ends

            } while(nextFrameStartTime < System.nanoTime());

            //render starts

            //find player coordinates in space (playerX is within the segment)
            double playerTime = player[OBJ_TIME];
            double playerX = playerTime - (int)playerTime;
            double[] road = segments[ROAD][(int)playerTime];
            double[] hill = segments[HILL][(int)playerTime];
            double playerY = (road[0] * playerX + road[1]) * playerX + road[2];
            double playerZ = (hill[0] * playerX + hill[1]) * playerX + hill[2] + 0.5f;

            //direction of road at player point
            double vx = 1;
            double vy = 2 * road[0] * playerX + road[1];
            mag = Math.sqrt(vx * vx + vy * vy);
            vx /= mag;
            vy /= mag;

            //draw road
            int height = 256;
            for(i = 0; i < 32; i++) {
                road = segments[ROAD][(int)playerTime + i];
                for(j = 0; j < 256; j++) {
                    double tx = I256 * j;

                    // coordinates of road point relative to player
                    double py = (road[0] * tx + road[1]) * tx + road[2] - playerY;
                    double pz = (hill[0] * tx + hill[1]) * tx + hill[2] - playerZ;
                    double px = tx - playerX + i;

                    //rotate point around z-axis using road direction as basis
                    double qx = vy * px - vx * py - player[OBJ_X];
                    double qy = vx * px - vy * py;

                    double K = 256 / (1 + qy);
                    int Y = 128 - (int)(K * pz);

                    //only draw a strip of road if it is not blocked by something else
                    if (Y < height) {
                        height = Y;

                        int X1 = 128 + (int)(K * (qx - 1.25f));
                        int X2 = 128 + (int)(K * (qx + 1.25f));

                        g.fillRect(0, Y, 256, 1);

                        g.drawImage(roadImages[(int)((tx - (int)tx) * 256)],
                                X1, Y, X2 - X1 + 1, 1, null);
                    }
                }
            }

            //render ends

            //show the hidden buffer
            if (g2 == null) {
                g2 = (Graphics2D)getGraphics();
                requestFocus();
            } else {
                g2.drawImage(image, 0, 0, 512, 512, null);
            }

            //burn off extra cycles
            while(nextFrameStartTime - System.nanoTime() > 0) {
                Thread.yield();
            }
        }
    }

    @Override
    public void processKeyEvent(KeyEvent keyEvent) {
        final int VK_LEFT = 0x25;
        final int VK_RIGHT = 0x27;
        final int VK_GAS = 0x44;
        final int VK_START = 0x20;
        final int VK_A = 0x41;
        final int VK_D = 0x44;

        int k = keyEvent.getKeyCode();
        if (k > 0) {
            if (k == VK_D) {
                k = VK_RIGHT;
            } else if (k == VK_A) {
                k = VK_LEFT;
            }
            a[(k == VK_LEFT || k == VK_RIGHT || k == VK_START) ? k : VK_GAS]
                    = keyEvent.getID() != 402;
        }
    }

    public static void main(String[] args) throws Throwable {
        javax.swing.JFrame frame = new javax.swing.JFrame("HighwayStar*");
        frame.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
        HighwayStar applet = new HighwayStar();
        applet.setPreferredSize(new java.awt.Dimension(512, 512));
        frame.add(applet, java.awt.BorderLayout.CENTER);
        frame.setResizable(false);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        Thread.sleep(250);
        applet.start();
    }


}
